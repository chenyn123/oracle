﻿<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

姓名：陈银    学号：201810414308    班级：20级软件工程3班

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|


## 总体设计方案
### 数据库表设计
表空间设计方案：  
表空间1：系统表空间（sale_sys）  
用于存储数据库的系统表和元数据信息。
大小根据实际需求进行设置，通常建议为较小的容量。  
表空间2：数据表空间（sale_data）  
用于存储用户数据表和索引。大小根据实际需求进行设置，需要足够的容量以容纳模拟数据量。  
表设计方案：  
1.商品（Products）  
字段：商品ID（ProductID）、商品名称（ProductName）、商品描述（ProductDescription）、商品价格（Price）、库存数量（StockQuantity）。  
2.客户（Customers）  
字段：客户ID（CustomerID）、客户姓名（CustomerName）、联系方式（Contact）、地址（Address）。
3.订单（Orders）  
字段：订单ID（OrderID）、客户ID（CustomerID）、订单日期（OrderDate）、订单总额（TotalAmount）。  
4.订单详情（OrderDetails）             
字段：订单详情ID（OrderDetailID）、订单ID（OrderID）、商品ID（ProductID）、数量（Quantity）、单价（UnitPrice）。

### 权限及用户分配设计
创建了两个用户：admin_user（管理员用户）和sales_user（销售员用户）。管理员用户被授予管理员角色，销售员用户被授予销售员角色。   
管理员角色（admin_role）被授予创建会话、创建表、创建序列和创建视图的权限。  
销售员角色（sales_role）被授予对商品、客户、订单和订单详情表的选择、插入、更新和删除权限。  
### 存储过程设计
calculate_customer_total 存储过程用于计算指定客户的订单总额，calculate_order_total 存储过程用于计算指定订单的总额和商品数量，get_stock_quantity 函数用于获取指定商品的库存数量，is_order_paid 函数用于检查指定订单是否已付款。

### 备份方案设计
使用 RMAN (Recovery Manager) 工具进行 Oracle 数据库备份和管理。


## 详细过程
### 创建用户,授予用户权限
```sql
--创建一个名为zar的用户，密码为123
GRANT USER zar IDENTIFIED BY 123;  
--授予zar权限
GRANT CONNECT,RESOURCE,CREATE SESSION TO zar;
```
第一个语句将名为"zar"的用户授予连接数据库的权限，并将其密码设置为"123"。第二个语句为用户授予了额外的权限，包括CONNECT、RESOURCE和CREATE SESSION  
![](image1.png)
### 创建表空间
```sql
-- 创建系统表空间
CREATE TABLESPACE sale_sys
  DATAFILE 'sale_sys.dbf' SIZE 100M
  AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
  LOGGING
  EXTENT MANAGEMENT LOCAL AUTOALLOCATE
  SEGMENT SPACE MANAGEMENT AUTO;
```
创建了一个名为"sale_sys"的系统表空间。它指定了一个名为"sale_sys.dbf"的数据文件，初始大小为100MB，并且允许自动扩展，每次扩展100MB，最大大小为无限。此外，它启用了日志记录、本地自动分配的区管理以及自动段空间管理。

```sql
-- 创建数据表空间
CREATE TABLESPACE sale_data
  DATAFILE 'sale_data.dbf' SIZE 500M
  AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
  LOGGING
  EXTENT MANAGEMENT LOCAL AUTOALLOCATE
  SEGMENT SPACE MANAGEMENT AUTO;
```
创建了一个名为"sale_data"的数据表空间。它指定了一个名为"sale_data.dbf"的数据文件，初始大小为500MB，并且允许自动扩展，每次扩展100MB，最大大小为无限。与系统表空间相似，它也启用了日志记录、本地自动分配的区管理以及自动段空间管理。  
![](image2.png)
### 创建表
```sql
-- 创建商品表
CREATE TABLE products (
  product_id       NUMBER PRIMARY KEY,
  product_name     VARCHAR2(100),
  product_desc     VARCHAR2(500),
  price            NUMBER(10, 2),
  stock_quantity   NUMBER
)
TABLESPACE sale_data;
```
创建了一个名为"products"的表。它包含了一些列，包括商品ID（product_id）、商品名称（product_name）、商品描述（product_desc）、价格（price）和库存数量（stock_quantity）。商品ID被定义为主键。该表将存储在名为"sale_data"的表空间中。
```sql
-- 创建客户表
CREATE TABLE customers (
  customer_id      NUMBER PRIMARY KEY,
  customer_name    VARCHAR2(100),
  contact          VARCHAR2(100),
  address          VARCHAR2(500)
)
TABLESPACE sale_data;
```
创建了一个名为"customers"的表。它包含了一些列，包括客户ID（customer_id）、客户名称（customer_name）、联系方式（contact）和地址（address）。客户ID被定义为主键。该表将存储在名为"sale_data"的表空间中。
```sql
-- 创建订单表
CREATE TABLE orders (
  order_id         NUMBER PRIMARY KEY,
  customer_id      NUMBER,
  order_date       DATE,
  total_amount     NUMBER(10, 2),
  CONSTRAINT fk_orders_customers FOREIGN KEY (customer_id)
    REFERENCES customers (customer_id)
)
TABLESPACE sale_data;
```
创建了一个名为"orders"的表。它包含了一些列，包括订单ID（order_id）、客户ID（customer_id）、订单日期（order_date）和总金额（total_amount）。订单ID被定义为主键，并且还创建了一个外键约束，将订单表中的customer_id列与customers表中的customer_id列关联起来。该表将存储在名为"sale_data"的表空间中。
```sql
-- 创建订单详情表
CREATE TABLE order_details (
  order_detail_id  NUMBER PRIMARY KEY,
  order_id         NUMBER,
  product_id       NUMBER,
  quantity         NUMBER,
  unit_price       NUMBER(10, 2),
  CONSTRAINT fk_order_details_orders FOREIGN KEY (order_id)
    REFERENCES orders (order_id),
  CONSTRAINT fk_order_details_products FOREIGN KEY (product_id)
    REFERENCES products (product_id)
)
TABLESPACE sale_data;
```
创建了一个名为"order_details"的表。它包含了一些列，包括订单详情ID（order_detail_id）、订单ID（order_id）、商品ID（product_id）、数量（quantity）和单价（unit_price）。订单详情ID被定义为主键，并且还创建了两个外键约束，将订单详情表中的order_id列与orders表中的order_id列关联起来，将order_details表中的product_id列与products表中的product_id列关联起来。该表将存储在名为"sale_data"的表空间中。
![](image3.png)
![](image4.png)
### 在表中插入100000条数据
```sql
-- 插入100,000条商品数据
INSERT INTO products (product_id, product_name, product_desc, price, stock_quantity)
SELECT
  product_seq.NEXTVAL,
  'Product ' || LEVEL,
  'Description ' || LEVEL,
  10.99,
  100
FROM
  dual
CONNECT BY
  LEVEL <= 100000;

-- 插入100,000条客户数据
INSERT INTO customers (customer_id, customer_name, contact, address)
SELECT
  customer_seq.NEXTVAL,
  'Customer ' || LEVEL,
  'Contact ' || LEVEL,
  'Address ' || LEVEL
FROM
  dual
CONNECT BY
  LEVEL <= 100000;

-- 插入100,000条订单数据
DECLARE
  customer_count NUMBER := 100000;
BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO orders (order_id, customer_id, order_date, total_amount)
    VALUES (order_seq.NEXTVAL, DBMS_RANDOM.VALUE(1, customer_count), SYSDATE, 100.00);
    COMMIT;
  END LOOP;
END;
/

-- 插入100,000条订单详情数据
DECLARE
  order_count NUMBER := 100000;
  product_count NUMBER := 100000;
BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO order_details (order_detail_id, order_id, product_id, quantity, unit_price)
    VALUES (order_detail_seq.NEXTVAL, DBMS_RANDOM.VALUE(1, order_count), DBMS_RANDOM.VALUE(1, product_count), 1, 10.99);
    COMMIT;
  END LOOP;
END;
/
```
使用了 `CONNECT BY` 子句和 `LEVEL` 伪列来生成100,000行数据，并使用适当的序列（例如 product_seq、customer_seq、order_seq、order_detail_seq）生成主键值。  
![](image5.png)
### 创建角色和权限分配
```sql
-- 创建角色
CREATE ROLE admin_role;
CREATE ROLE sales_role;
```
创建两个角色一个管理员角色（admin_role），一个销售员角色（sales_role）
![](image6.png)
```sql
-- 授予管理员角色权限
GRANT CREATE SESSION TO admin_role;
GRANT CREATE TABLE TO admin_role;
GRANT CREATE SEQUENCE TO admin_role;
GRANT CREATE VIEW TO admin_role;

-- 授予销售员角色权限
GRANT CREATE SESSION TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO sales_role;
```
管理员角色（admin_role）被授予创建会话、创建表、创建序列和创建视图的权限。  
销售员角色（sales_role）被授予对商品、客户、订单和订单详情表的选择、插入、更新和删除权限。
![](iamge7.png)
![](iamge8.png)
### 创建程序包
创建了一个名为 sales_pkg 的程序包。程序包中包含了四个子程序：两个存储过程和两个函数。
(1)calculate_customer_total 存储过程接受一个客户ID作为输入参数，并通过该客户ID查询订单表，计算该客户的订单总额。计算结果存储在输出参数 p_total_amount 中。    
(2)calculate_order_total 存储过程接受一个订单ID作为输入参数，并通过该订单ID查询订单详情表，计算该订单的总额和商品数量。计算结果分别存储在输出参数 p_total_amount 和 p_total_quantity 中。  
(3)get_stock_quantity 函数接受一个产品ID作为输入参数，并通过该产品ID查询产品表，获取该产品的库存数量，并返回结果。  
(4)is_order_paid 函数接受一个订单ID作为输入参数，并通过该订单ID查询订单表，检查订单是否已付款。如果订单的总金额大于0，则返回 TRUE，表示订单已付款；否则返回 FALSE，表示订单未付款。
```sql
-- 创建程序包
CREATE OR REPLACE PACKAGE sales_pkg IS
  -- 存储过程：根据客户ID计算客户的订单总额
  PROCEDURE calculate_customer_total(
    p_customer_id IN NUMBER,
    p_total_amount OUT NUMBER
  );

  -- 存储过程：根据订单ID计算订单的总额和商品数量
  PROCEDURE calculate_order_total(
    p_order_id IN NUMBER,
    p_total_amount OUT NUMBER,
    p_total_quantity OUT NUMBER
  );

  -- 函数：获取库存数量
  FUNCTION get_stock_quantity(
    p_product_id IN NUMBER
  ) RETURN NUMBER;

  -- 函数：检查订单是否已付款
  FUNCTION is_order_paid(
    p_order_id IN NUMBER
  ) RETURN BOOLEAN;
END sales_pkg;
/

-- 创建程序包体
CREATE OR REPLACE PACKAGE BODY sales_pkg IS
  -- 存储过程：根据客户ID计算客户的订单总额
  PROCEDURE calculate_customer_total(
    p_customer_id IN NUMBER,
    p_total_amount OUT NUMBER
  ) IS
  BEGIN
    SELECT SUM(o.total_amount)
    INTO p_total_amount
    FROM orders o
    WHERE o.customer_id = p_customer_id;
  END calculate_customer_total;

  -- 存储过程：根据订单ID计算订单的总额和商品数量
  PROCEDURE calculate_order_total(
    p_order_id IN NUMBER,
    p_total_amount OUT NUMBER,
    p_total_quantity OUT NUMBER
  ) IS
  BEGIN
    SELECT SUM(od.unit_price * od.quantity), SUM(od.quantity)
    INTO p_total_amount, p_total_quantity
    FROM order_details od
    WHERE od.order_id = p_order_id;
  END calculate_order_total;

  -- 函数：获取库存数量
  FUNCTION get_stock_quantity(
    p_product_id IN NUMBER
  ) RETURN NUMBER IS
    l_stock_quantity NUMBER;
  BEGIN
    SELECT stock_quantity
    INTO l_stock_quantity
    FROM products
    WHERE product_id = p_product_id;

    RETURN l_stock_quantity;
  END get_stock_quantity;

  -- 函数：检查订单是否已付款
  FUNCTION is_order_paid(
    p_order_id IN NUMBER
  ) RETURN BOOLEAN IS
    l_total_amount NUMBER;
  BEGIN
    SELECT total_amount
    INTO l_total_amount
    FROM orders
    WHERE order_id = p_order_id;

    RETURN l_total_amount > 0;
  END is_order_paid;
END sales_pkg;
/
```
![](iamge9.png)
### 调用存储过程
```sql
SET SERVEROUTPUT ON;
-- 调用存储过程：根据客户ID计算客户的订单总额
DECLARE
  total_amount NUMBER;
BEGIN
  sales_pkg.calculate_customer_total(1, total_amount); -- 传入客户ID和输出参数
  DBMS_OUTPUT.PUT_LINE('Customer Total Amount: ' || total_amount);
END;
/

-- 调用存储过程：根据订单ID计算订单的总额和商品数量
DECLARE
  total_amount NUMBER;
  total_quantity NUMBER;
BEGIN
  sales_pkg.calculate_order_total(1, total_amount, total_quantity); -- 传入订单ID和输出参数
  DBMS_OUTPUT.PUT_LINE('Order Total Amount: ' || total_amount);
  DBMS_OUTPUT.PUT_LINE('Order Total Quantity: ' || total_quantity);
END;
/

-- 调用函数：获取库存数量
DECLARE
  stock_quantity NUMBER;
BEGIN
  stock_quantity := sales_pkg.get_stock_quantity(503); -- 传入产品ID
  DBMS_OUTPUT.PUT_LINE('Stock Quantity: ' || stock_quantity);
END;
/

-- 调用函数：检查订单是否已付款
DECLARE
  is_paid BOOLEAN;
BEGIN
  is_paid := sales_pkg.is_order_paid(1000); -- 传入订单ID
  IF is_paid THEN
    DBMS_OUTPUT.PUT_LINE('Order is paid.');
  ELSE
    DBMS_OUTPUT.PUT_LINE('Order is not paid.');
  END IF;
END;
/
```
![](iamge10.png)
### 备份方案
```sql
-- 登录到数据库
sqlplus / as sysdba

-- 关闭数据库
SHUTDOWN IMMEDIATE

-- 启动数据库实例并挂载数据库
STARTUP MOUNT

-- 将数据库切换到归档日志模式
ALTER DATABASE ARCHIVELOG；

-- 打开数据库
ALTER DATABASE OPEN；
```
![](image11.png)
```sql
-- 使用 RMAN 工具进行备份
rman target /

-- 显示 RMAN 的当前配置和参数设置
SHOW ALL;

-- 执行完整数据库备份操作
BACKUP DATABASE;

-- 列出已备份的备份集信息
LIST BACKUP;
