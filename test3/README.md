# 实验3：创建分区表
姓名：陈银   学号：201810414308

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验参考

- 使用sql-developer软件创建表，并导出类似以下的脚本。
- 以下脚本不含orders.customer_name的索引，不含序列设置，仅供参考。

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
--以后再逐年增加新年份的分区
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;

```
创建了一个名为"orders"的表，其中包含了订单的相关信息，包括订单ID、客户名称、客户电话、下单日期、员工ID、折扣和交易应收款等。

这个表采用了基于订单日期的分区方式，将订单按照年份进行分区管理，其中包括三个已经定义好的分区，分别是在2016年之前、2016年到2020年之间以及2020年到2021年之间的订单。同时，这段代码还定义了一个将在2022年之前的订单加入到名为"partition_before_2022"的分区中。

这个表的数据将存储在名为"USERS"的表空间中，初始空间占用率为10%，并且不采用压缩和并行处理方式。每个分区的初始化大小为8MB，下一个扩展大小为1MB，并且可以无限扩展，不采用In-Memory技术。
![](img1.png)

- 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```
创建了一个名为"order_details"的表，用于存储订单中的商品详细信息。该表包含了商品详细信息的ID、订单ID、商品ID、商品数量和商品单价等字段。

其中，"id"是该表的主键，并且"order_id"字段是一个外键，引用了"orders"表中的"order_id"字段。这个外键关系被启用，并且在"order_details_fk1"分区键的引导下进行引用分区操作。

这个表的数据将存储在名为"USERS"的表空间中，初始空间占用率为10%，并且不采用压缩和并行处理方式。每个分区的初始化大小为8MB，并且下一个扩展大小为1MB。这个表使用了基于引用分区的方式来进行分区管理，即它的分区方式和"orders"表中的分区方式相同。
![](img2.png)

- 创建序列SEQ1的语句如下

```sql
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```
![](img3.png)

- 插入100条orders记录的样例脚本如下：

```sql
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
/

说明：
|| 表示字符连接符号
SEQ1是一个序列对象
```
这段PL/SQL代码是一个简单的循环结构，用于向"orders"表中插入100条记录，每条记录包含一个自动生成的订单ID和订单日期。

循环的初始值为i=0，y=2015，m=1和d=12，然后每次循环i增加1，m增加1。当m大于12时，将其重置为1。将y、m和d的值拼接成一个字符串，然后使用to_date函数将其转换为日期格式。最后使用insert语句向"orders"表中插入一条记录，包含一个自动生成的订单ID和日期。

在循环结束后，使用commit语句提交所有的插入操作，使其生效。这段代码中使用了一个名为"SEQ1"的序列来生成订单ID。
![](img4.png)
```sql
SELECT order_id, customer_name, order_date, NULL AS product_id, NULL AS product_num, NULL AS product_price
FROM orders
UNION ALL
SELECT order_id, NULL AS customer_name, NULL AS order_date, product_id, product_num, product_price
FROM order_details;
```
![](img5.png)
```sql
EXPLAIN PLAN FOR
SELECT order_id, customer_name, order_date, NULL AS product_id, NULL AS product_num, NULL AS product_price
FROM orders
UNION ALL
SELECT order_id, NULL AS customer_name, NULL AS order_date, product_id, product_num, product_price
FROM order_details;
SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY)
```
![](img6.png)
## 实验总结
本次实验主要是学习了分区表的创建与使用。分区表可以将数据按照一定规则分散到多个分区中存储，从而提升查询效率，提高系统的稳定性和性能。在实验中，我们创建了两个分区表：订单表和订单详表，并且分别设置了范围分区和引用分区。

在创建分区表时，需要注意以下几点：

1.主键必须匹配分区键，才能使用分区特性。
2.分区表必须至少有一个分区键。
3.分区表的分区键索引是必须的。
4.分区表的分区键数据类型必须匹配索引类型。
5.在创建分区表时，应该设置各个分区的边界，以确保数据能够均匀地分布到各个分区中。

在实验过程中，我们还学习了索引和序列的使用。索引可以提高查询效率，而序列可以在插入数据时自动生成唯一的ID标识符，方便管理和查询数据。

最后，我们进行了分区与不分区的对比实验，通过比较查询数据的执行计划，发现分区表的查询效率优于非分区表。

总的来说，本次实验让我深入了解了分区表的创建和使用，并掌握了索引和序列的使用，对于提高系统性能和运行稳定性有了更深刻的理解和实践。